"use strict";

const themeBtn = document.createElement("button");
themeBtn.textContent = "Сменить тему";

if (localStorage.getItem("theme") === null) {
  let colors = {
    bodyMain: "white",
    menuColor: "white",
    menuTopBcg: "#35444F",
    sideMenu: "#485863",
    text: "#2C2C2C",
    botMenu: "black",
    botNav: "rgba(99, 105, 110, 0.48)",
    copyright: "black",
  };

  let colToStorage = JSON.stringify(colors);
  // console.log(colors);

  localStorage.setItem("theme", colToStorage);
}

let theme = JSON.parse(localStorage.getItem("theme"));

themeChange(theme);

themeBtn.addEventListener("click", function () {
  if (
    localStorage.getItem("theme") === null ||
    JSON.parse(localStorage.getItem("theme")).bodyMain === "white"
  ) {
    let colors = {
      bodyMain: "grey",
      menuColor: "lightskyblue",
      menuTopBcg: "#black",
      sideMenu: "lightskyblue",
      text: "lightskyblue",
      botMenu: "lightskyblue",
      botNav: "black",
      copyright: "lightskyblue",
    };

    let colToStorage = JSON.stringify(colors);
    // console.log(colors);

    localStorage.setItem("theme", colToStorage);
  } else {
    let colors = {
      bodyMain: "white",
      menuColor: "white",
      menuTopBcg: "#35444F",
      sideMenu: "#485863",
      text: "#2C2C2C",
      botMenu: "black",
      botNav: "rgba(99, 105, 110, 0.48)",
      copyright: "black",
    };

    let colToStorage = JSON.stringify(colors);
    // console.log(colors);

    localStorage.setItem("theme", colToStorage);
  }

  let theme = JSON.parse(localStorage.getItem("theme"));
  themeChange(theme);
});

document.getElementsByTagName("aside")[0].append(themeBtn);

function themeChange(theme) {
  document.getElementsByClassName(
    "body__main"
  )[0].style.backgroundColor = `${theme.bodyMain}`;
  document.getElementsByClassName("menu")[0].style.color = `${theme.menuColor}`;
  document.getElementsByClassName(
    "menu"
  )[0].style.backgroundColor = `${theme.menuTopBcg}`;

  document.getElementsByClassName(
    "side_menu"
  )[0].style.color = `${theme.sideMenu}`;

  document.getElementsByClassName("text")[0].style.color = `${theme.text}`;
  document.getElementsByClassName("text")[1].style.color = `${theme.text}`;

  document.getElementsByClassName(
    "bot_menu"
  )[0].style.color = `${theme.botMenu}`;
  document.getElementsByClassName(
    "bottom_nav"
  )[0].style.backgroundColor = `${theme.botNav}`;

  document.getElementsByClassName(
    "copyright"
  )[0].style.color = `${theme.copyright}`;
}
